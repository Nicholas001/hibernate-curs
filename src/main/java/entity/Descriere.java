package entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity

@Table(name = "descriere")
public class Descriere implements Serializable {

    private final static String DESCRIERE_SEQUENCE = "descriere_id_seq";

    @Id
    @SequenceGenerator(name = "desciere_generator", sequenceName = DESCRIERE_SEQUENCE)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "descriere_generator")
    private int id;

    @Column(name = "culoare")
    private String culoare;

    @Column(name = "conditiiDePatrare")
    private String conditiiDePatrare;

    @Column(name = "stoc")
    private int stoc;

    @OneToOne
    @JoinColumn(name = "produs_id")
    private Produs produs;

    public Descriere() {
    }

    public String getCuloare() {
        return culoare;
    }

    public void setCuloare(String culoare) {
        this.culoare = culoare;
    }

    public String getConditiiDePatrare() {
        return conditiiDePatrare;
    }

    public void setConditiiDePatrare(String conditiiDePatrare) {
        this.conditiiDePatrare = conditiiDePatrare;
    }

    public int getStoc() {
        return stoc;
    }

    public void setStoc(int stoc) {
        this.stoc = stoc;
    }

    public Produs getProdus() {
        return produs;
    }

    public void setProdus(Produs produs) {
        this.produs = produs;
    }

    @Override
    public String toString() {
        return "Descriere{" +
                "id=" + id +
                ", culoare='" + culoare + '\'' +
                ", conditiiDePatrare='" + conditiiDePatrare + '\'' +
                ", stoc=" + stoc +
                ", produs=" + produs +
                '}';
    }
}
