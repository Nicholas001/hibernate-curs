package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity

@NamedQueries({
        @NamedQuery(
                name = "find_by_name",
                query = "select p from Produs p where name = :name "
        ),
        @NamedQuery(
                name = "update_produs_identified_by_id",
                query = "update Produs p set name =: name where id =: id"
        ),
        @NamedQuery(
                name = "delete produs_identified_by_id",
                query = "delete Produs p where id =: id"
        )
})

@Table(name = "produs")
public class Produs implements Serializable {

    private final static String PRODUS_SEQUENCE = "produs_id_seq";


    @Id
    @SequenceGenerator(name = "produs_generator", sequenceName = PRODUS_SEQUENCE)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "produs_generator")
    private int id;

    @Column(name = "name", unique = false)
    private String name;

    @Column(name = "price")
    private int price;

    @Column(name = "expiredate")
    private Date expireDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Descriere descriere;

    public Produs() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Descriere getDescriere() {
        return descriere;
    }

    public void setDescriere(Descriere descriere) {
        this.descriere = descriere;
    }

    @Override
    public String toString() {
        return "Produs{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", expireDate=" + expireDate +
                '}';
    }
}
