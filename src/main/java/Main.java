import db.DbDescriere;
import db.DbProdus;
import entity.Descriere;
import entity.Produs;

import java.util.Arrays;
import java.util.Date;

public class Main {

    public static void main(String[] args) {

        Produs produs = new Produs();

        produs.setName("Morcovi");
        produs.setPrice(36);
        produs.setExpireDate(new Date((System.currentTimeMillis())));
        Descriere descriere = new Descriere();
        descriere.setCuloare("rosu");
        descriere.setConditiiDePatrare("Uscat");
        descriere.setStoc(70);
        descriere.setProdus(produs);
        produs.setDescriere(descriere);

        DbProdus dbProdus = new DbProdus();

        DbDescriere dbDescriere = new DbDescriere();
        System.out.println(dbDescriere.getDescriere(52));
//        dbProdus.insertProdus(produs);
//        dbProdus.deleteProdusById(produs);
//        dbProdus.updateProdus(produs);

//        dbProdus.deleteProdus(152);
//        System.out.println(dbProdus.getProdus(352));


//        Produs produs2 = new Produs();
//        produs2.setName("castravetei");
//        produs2.setPrice(20);
//        produs2.setExpireDate(new Date(System.currentTimeMillis()));
//
//        Produs produs3 = new Produs();
//        produs3.setName("rosie");
//        produs3.setPrice(15);
//        produs3.setExpireDate(new Date(System.currentTimeMillis()));

//        dbProdus.insertProdus(Arrays.asList(produs, produs2, produs3));

//        System.out.println(dbProdus.getProdusByName("rosie"));

    }

}
