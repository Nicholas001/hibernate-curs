package db;

import entity.Descriere;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DbDescriere extends DbInitializer {


    private Session session;
    private Transaction transaction;

    public Descriere getDescriere(int id) {
        getSessionAndTransaction();
        Descriere descriere = session.find(Descriere.class, id);
        commitTransactionAndCloseSession();
        return descriere;
    }


    public void getSessionAndTransaction() {
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();
    }

    public void commitTransactionAndCloseSession() {
        transaction.commit();
        session.close();
    }

}
