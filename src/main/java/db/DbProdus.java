package db;

import entity.Produs;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.List;

public class DbProdus extends DbInitializer {

    private Session session;
    private Transaction transaction;

    public void getSessionAndTransaction() {
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();
    }

    public void commitTransactionAndCloseSession() {
        transaction.commit();
        session.close();
    }

    public void insertProdus(Produs produs) {

        getSessionAndTransaction();
//
//        for (Produs produs : produse) {
//            try {
//                session.persist(produs);
//            } catch (Exception e) {
//                transaction.rollback();
//            }
//        }
//        commitTransactionAndCloseSession();
        session.persist(produs);
//        session.detach(produs);
//        produs.setName("rosie");
        commitTransactionAndCloseSession();
    }

    public void deleteProdus(int id) {
        getSessionAndTransaction();
        Produs produs = session.find(Produs.class, id);
        session.delete(produs);
        commitTransactionAndCloseSession();
    }

    public Produs getProdus(int id) {
        getSessionAndTransaction();
        Produs produs = session.find(Produs.class, id);
        commitTransactionAndCloseSession();
        return produs;
    }


    public Produs getProdusByName(String name) {
        getSessionAndTransaction();
        Query query = session.createNamedQuery("find_by_name");
        query.setParameter("name", name);
        Produs produs = (Produs) query.getSingleResult();
        commitTransactionAndCloseSession();
        return produs;
    }

    public void updateProdus(Produs produs) {
        getSessionAndTransaction();
//        Produs produsvechi = session.find(Produs.class, produs.getId());
//        produsvechi.setName(produs.getName());
//        produsvechi.setPrice(produs.getPrice());
        session.merge(produs);
        commitTransactionAndCloseSession();
    }

    public void updateProdusById(Produs produsNou) {
        getSessionAndTransaction();
        Query query = session.createNamedQuery("update_produs_identified_by_id");
        query.setParameter("id", produsNou.getId()).setParameter("name", produsNou.getName() );
        query.executeUpdate();
        commitTransactionAndCloseSession();
    }

    public void deleteProdusById (Produs produs){
        getSessionAndTransaction();
        Query query = session.createNamedQuery("delete produs_identified_by_id");
        query.setParameter("id", produs.getId());
        query.executeUpdate();
        commitTransactionAndCloseSession();
    }

}
